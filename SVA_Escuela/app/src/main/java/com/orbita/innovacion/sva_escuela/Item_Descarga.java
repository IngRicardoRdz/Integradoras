package com.orbita.innovacion.sva_escuela;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

public class Item_Descarga {

    private String img;
    private String ID;
    private String titulo;
    private String contenido;

    public Item_Descarga(String titulo, String contenido, String img, String id) {
        this.img = img;
        this.ID = id;
        this.titulo = titulo;
        this.contenido = contenido;
    }

    public Bitmap getImg() {
        return base64ToBitmap(img);
    }

    public String getTitulo() {
        return titulo;
    }

    public String getContenido() {
        return contenido;
    }

    public String getID() {
        return ID;
    }

    private Bitmap base64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }

}
