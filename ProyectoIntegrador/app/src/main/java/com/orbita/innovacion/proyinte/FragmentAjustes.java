package com.orbita.innovacion.proyinte;

import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.google.firebase.messaging.FirebaseMessaging;

/**
 * Clase de Fragmento encargada de...
 *
 * Fecha de creación: 01/03/2018.
 * Versión: 18.3.01
 * Modificaciones:
 * Ninguna.
 */

public class FragmentAjustes extends Fragment {

    private String PINPassword = null;
    private Button suscribirse, cambiarpin;
    private String tema;
    private View v;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_fragment_ajustes, container, false);

        tema = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("Tema","null");

        suscribirse = (Button) v.findViewById(R.id.btn_suscribirse);
        cambiarpin = (Button) v.findViewById(R.id.btn_pin);

        suscribirse.setVisibility(View.VISIBLE);

        final String suscrito = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("Suscrito","NO");
        if(suscrito.toString().equals("NO")){
            suscribirse.setText("Suscribirse a su Grupo");
        }else{
            suscribirse.setText("Desucribirse de su Grupo");
        }

        cambiarpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CambiarPin();
            }
        });

        suscribirse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(suscribirse.getText().toString().equals("Suscribirse a su Grupo")){
                    Suscribirse();
                }else{
                    Desuscribirse();
                }
            }
        });

        return v;
    }

    private void Suscribirse() {
        Log.v("Suscrito",tema);
        FirebaseMessaging.getInstance().subscribeToTopic(tema);

        suscribirse.setText("Desuscribirse de su Grupo");

        Snackbar.make(v.findViewById(R.id.LayoutAjustes), "Suscripción correcta",
                Snackbar.LENGTH_LONG).setAction("", null).show();

        SharedPreferences("Suscrito", "SI");
    }

    private void Desuscribirse() {
        ventanaDialogo();
    }

    private void CambiarPin() {
        android.support.v4.app.FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fram, new pin_cambio()).commit();
    }

    private void SharedPreferences(String nombre, String dato){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(nombre);
        editor.putString(nombre, dato);
        editor.apply();
    }

    public void ventanaDialogo()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Suscripción");
        builder.setMessage("¿Seguro/a que desea desuscribirse?");

        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Snackbar.make(v.findViewById(R.id.LayoutAjustes), "Acción Desechada",
                                Snackbar.LENGTH_LONG).setAction("", null).show();

                    }
                });

        builder.setPositiveButton("Si",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Log.v("Desuscrito",tema);
                        FirebaseMessaging.getInstance().unsubscribeFromTopic(tema);

                        suscribirse.setVisibility(View.GONE);

                        Snackbar.make(v.findViewById(R.id.LayoutAjustes), "Desuscripción correcta",
                                Snackbar.LENGTH_LONG).setAction("", null).show();

                        SharedPreferences("Suscrito", "NO");

                    }
                });
        builder.show();
    }

}
