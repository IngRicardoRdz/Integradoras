package com.orbita.innovacion.proyinte;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Clase Fragmento encargada del consumo de la Api de Google Maps para la visualización de
 * papelerias o tiendas cercanas a la institución implementada.
 *
 * Fecha de creación: 17/01/2018
 * Versión: 11.7.36
 * Modificaciones:
 * Enfoque en la localizacion del plantel 18/01/2018
 */

public class MapsActivity extends Fragment implements OnMapReadyCallback {

    private GoogleMap Map;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_maps, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SupportMapFragment supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Map = googleMap;

        LatLng primaria = new LatLng(28.647889, -106.0260268);
        Map.addMarker(new MarkerOptions().position(primaria).title("Escuela Primaria"));
        Map.moveCamera(CameraUpdateFactory.newLatLngZoom(primaria, 18));

        LatLng merceria1 = new LatLng(28.6481866, -106.0260503);
        Map.addMarker(new MarkerOptions().position(merceria1).title("Merceria Caro"));

        LatLng merceria2 = new LatLng(28.649374, -106.025473);
        Map.addMarker(new MarkerOptions().position(merceria2).title("Papeleria Mireya"));

        LatLng tienda1 = new LatLng(28.6487193, -106.0254111);
        Map.addMarker(new MarkerOptions().position(tienda1).title("Tienda Mireya"));

        LatLng oxxo1 = new LatLng(28.6497341, -106.028498);
        Map.addMarker(new MarkerOptions().position(oxxo1).title("Tienda OXXO"));

        LatLng papeleria1 = new LatLng(28.6498442, -106.0294919);
        Map.addMarker(new MarkerOptions().position(papeleria1).title("Papeleria Jade"));
    }

}
