package com.orbita.innovacion.proyinte;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 *Clase de Fragmento encargada de poder visualizar la pagina web que se le pondra a cada escuela.
 *
 * Fecha de creación: 08/04/2018.
 * Versión: 18.4.04.release
 *
 * Modificaciones:
 * Ninguna.
 */

public class FragmentPagina extends Fragment {

    WebView mWebView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fragment_pagina, container, false);

        mWebView = (WebView) v.findViewById(R.id.webView);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.loadUrl("http://demo1234demostracion.000webhostapp.com/");

        return v;
    }

}
